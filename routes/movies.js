const auth = require('../middlewares/auth')
const { Movie, validate } = require('../models/movie')
const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()

router.get('/', async (req, res) => {
  const movies = await Movie.find().sort('title')
  res.send(movies)
})

router.get('/:id', async (req, res) => {
  const movie = await Movie.findById(req.params.id)
  if(!movie) return res.status(404).send('No movie for the given id')
  res.send(movie)
})

router.post('/', auth, async (req, res) => {
  const { error } = validate(req.body)
  if (error) return res.status(400).send(error.details[0].message)

  const genre = await Genre.finById(req.body.genreId)
  if(!genre) return res.status(404).send('Invalid genre')

  const movie = new Movie({
    title: req.body.title,
    numberInStock: req.body.numberInStock,
    dailyRentalRate: req.body.dailyRentalRate,
    genre: {
      _id: genre._id,
      name: genre.name
    }
  })

  await Movie.save()
  res.send(movie)
})

router.put('/:id', auth, async (req, res) => {
  const { error } = validate(req.body)
  if (error) return res.status(400).send(error.details[0].message)

  const updatedMovie = {
    title: req.body.title,
    numberInStock: req.body.numberInStock,
    dailyRentalRate: req.body.dailyRentalRate,
    genre: {
      _id: genre._id,
      name: genre.name
    }
  }
  const movie = await Movie.findByIdAndUpdate(req.params.id, updatedMovie, { new: true })
  if(!movie) return res.status(404).send('No movie for the given id')
  res.send(movie)
})

router.delete('/:id', auth, async (req, res) => {
  const movie = await Movie.findByIdAndRemove(req.params.id)
  if(!movie) return res.status(404).send('No movie for the given id')
  res.send(movie)
})

module.exports = router